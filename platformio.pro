win32 {
    HOMEDIR += $$(USERPROFILE)
}
else {
    HOMEDIR += $$(HOME)
}

INCLUDEPATH += "$${HOMEDIR}/.platformio/packages/framework-arduinoststm32/STM32F1/cores/maple"
INCLUDEPATH += "$${HOMEDIR}/.platformio/packages/framework-arduinoststm32/STM32F1/system/libmaple"
INCLUDEPATH += "$${HOMEDIR}/.platformio/packages/framework-arduinoststm32/STM32F1/system/libmaple/include"
INCLUDEPATH += "$${HOMEDIR}/.platformio/packages/framework-arduinoststm32/STM32F1/system/libmaple/usb/stm32f1"
INCLUDEPATH += "$${HOMEDIR}/.platformio/packages/framework-arduinoststm32/STM32F1/system/libmaple/usb/usb_lib"
INCLUDEPATH += "$${HOMEDIR}/.platformio/packages/framework-arduinoststm32/STM32F1/variants/generic_stm32f103c"
INCLUDEPATH += "/home_extra/v01d/coding/bicycle-companion-mini/.piolibdeps/Button_ID77"
INCLUDEPATH += "$${HOMEDIR}/.platformio/packages/framework-arduinoststm32/STM32F1/libraries/FreeRTOS"
INCLUDEPATH += "$${HOMEDIR}/.platformio/packages/framework-arduinoststm32/STM32F1/libraries/FreeRTOS/utility"
INCLUDEPATH += "/home_extra/v01d/coding/bicycle-companion-mini/.piolibdeps/Time_ID44"
INCLUDEPATH += "/home_extra/v01d/coding/bicycle-companion-mini/.piolibdeps/OneWire_ID1"
INCLUDEPATH += "/home_extra/v01d/coding/bicycle-companion-mini/.piolibdeps/DallasTemperature_ID54"
INCLUDEPATH += "/home_extra/v01d/coding/bicycle-companion-mini/lib/Adafruit-GFX-Library"
INCLUDEPATH += "$${HOMEDIR}/.platformio/packages/framework-arduinoststm32/STM32F1/libraries/SPI/src"
INCLUDEPATH += "$${HOMEDIR}/.platformio/packages/framework-arduinoststm32/STM32F1/libraries/Wire"
INCLUDEPATH += "/home_extra/v01d/coding/bicycle-companion-mini/lib/Adafruit_SSD1306"
INCLUDEPATH += "/home_extra/v01d/coding/bicycle-companion-mini/lib/MenuSystem"
INCLUDEPATH += "/home_extra/v01d/coding/bicycle-companion-mini/lib/Metro"
INCLUDEPATH += "/home_extra/v01d/coding/bicycle-companion-mini/lib/STM32_Sleep"
INCLUDEPATH += "/home_extra/v01d/coding/bicycle-companion-mini/src"
INCLUDEPATH += "/home_extra/v01d/coding/bicycle-companion-mini/lib/Adafruit-GFX-Library"
INCLUDEPATH += "/home_extra/v01d/coding/bicycle-companion-mini/lib/Adafruit_SSD1306"
INCLUDEPATH += "/home_extra/v01d/coding/bicycle-companion-mini/lib/MenuSystem"
INCLUDEPATH += "/home_extra/v01d/coding/bicycle-companion-mini/lib/MenuSystem"
INCLUDEPATH += "/home_extra/v01d/coding/bicycle-companion-mini/.piolibdeps/Button_ID77"
INCLUDEPATH += "/home_extra/v01d/coding/bicycle-companion-mini/.piolibdeps/Button_ID77"
INCLUDEPATH += "/home_extra/v01d/coding/bicycle-companion-mini/.piolibdeps/DallasTemperature_ID54"
INCLUDEPATH += "$${HOMEDIR}/.platformio/packages/framework-arduinoststm32/STM32F1/libraries/FreeRTOS"
INCLUDEPATH += "$${HOMEDIR}/.platformio/packages/framework-arduinoststm32/STM32F1/libraries/FreeRTOS/utility"
INCLUDEPATH += "/home_extra/v01d/coding/bicycle-companion-mini/.piolibdeps/OneWire_ID1"
INCLUDEPATH += "/home_extra/v01d/coding/bicycle-companion-mini/.piolibdeps/DallasTemperature_ID54"
INCLUDEPATH += "/home_extra/v01d/coding/bicycle-companion-mini/.piolibdeps/OneWire_ID1"
INCLUDEPATH += "$${HOMEDIR}/.platformio/packages/framework-arduinoststm32/STM32F1/libraries/FreeRTOS"
INCLUDEPATH += "$${HOMEDIR}/.platformio/packages/framework-arduinoststm32/STM32F1/libraries/FreeRTOS/utility"
INCLUDEPATH += "/home_extra/v01d/coding/bicycle-companion-mini/.piolibdeps/OneWire_ID1"
INCLUDEPATH += "$${HOMEDIR}/.platformio/lib/Button_ID77"
INCLUDEPATH += "$${HOMEDIR}/.platformio/packages/framework-arduinoststm32/STM32F1/libraries/A_STM32_Examples"
INCLUDEPATH += "$${HOMEDIR}/.platformio/packages/framework-arduinoststm32/STM32F1/libraries/Adafruit_GFX_AS"
INCLUDEPATH += "$${HOMEDIR}/.platformio/packages/framework-arduinoststm32/STM32F1/libraries/Adafruit_ILI9341"
INCLUDEPATH += "$${HOMEDIR}/.platformio/packages/framework-arduinoststm32/STM32F1/libraries/Adafruit_ILI9341_STM"
INCLUDEPATH += "$${HOMEDIR}/.platformio/packages/framework-arduinoststm32/STM32F1/libraries/Adafruit_SSD1306"
INCLUDEPATH += "$${HOMEDIR}/.platformio/packages/framework-arduinoststm32/STM32F1/libraries/EEPROM"
INCLUDEPATH += "$${HOMEDIR}/.platformio/packages/framework-arduinoststm32/STM32F1/libraries/Ethernet_STM/src"
INCLUDEPATH += "$${HOMEDIR}/.platformio/packages/framework-arduinoststm32/STM32F1/libraries/FreeRTOS"
INCLUDEPATH += "$${HOMEDIR}/.platformio/packages/framework-arduinoststm32/STM32F1/libraries/FreeRTOS/utility"
INCLUDEPATH += "$${HOMEDIR}/.platformio/packages/framework-arduinoststm32/STM32F1/libraries/FreeRTOS821"
INCLUDEPATH += "$${HOMEDIR}/.platformio/packages/framework-arduinoststm32/STM32F1/libraries/FreeRTOS821/utility"
INCLUDEPATH += "$${HOMEDIR}/.platformio/packages/framework-arduinoststm32/STM32F1/libraries/ILI9341_due_STM"
INCLUDEPATH += "$${HOMEDIR}/.platformio/packages/framework-arduinoststm32/STM32F1/libraries/Lcd7920_STM"
INCLUDEPATH += "$${HOMEDIR}/.platformio/packages/framework-arduinoststm32/STM32F1/libraries/LiquidCrystal"
INCLUDEPATH += "$${HOMEDIR}/.platformio/packages/framework-arduinoststm32/STM32F1/libraries/MapleCoOS"
INCLUDEPATH += "$${HOMEDIR}/.platformio/packages/framework-arduinoststm32/STM32F1/libraries/MapleCoOS/utility"
INCLUDEPATH += "$${HOMEDIR}/.platformio/packages/framework-arduinoststm32/STM32F1/libraries/MapleCoOS116"
INCLUDEPATH += "$${HOMEDIR}/.platformio/packages/framework-arduinoststm32/STM32F1/libraries/MapleCoOS116/utility"
INCLUDEPATH += "$${HOMEDIR}/.platformio/packages/framework-arduinoststm32/STM32F1/libraries/OLED_I2C"
INCLUDEPATH += "$${HOMEDIR}/.platformio/packages/framework-arduinoststm32/STM32F1/libraries/OneWireSTM/src"
INCLUDEPATH += "$${HOMEDIR}/.platformio/packages/framework-arduinoststm32/STM32F1/libraries/RTClock/src"
INCLUDEPATH += "$${HOMEDIR}/.platformio/packages/framework-arduinoststm32/STM32F1/libraries/SPI/src"
INCLUDEPATH += "$${HOMEDIR}/.platformio/packages/framework-arduinoststm32/STM32F1/libraries/Serasidis_EtherCard_STM/src"
INCLUDEPATH += "$${HOMEDIR}/.platformio/packages/framework-arduinoststm32/STM32F1/libraries/Serasidis_VS1003B_STM/src"
INCLUDEPATH += "$${HOMEDIR}/.platformio/packages/framework-arduinoststm32/STM32F1/libraries/Serasidis_XPT2046_touch/src"
INCLUDEPATH += "$${HOMEDIR}/.platformio/packages/framework-arduinoststm32/STM32F1/libraries/Servo"
INCLUDEPATH += "$${HOMEDIR}/.platformio/packages/framework-arduinoststm32/STM32F1/libraries/Touch-Screen-Library_STM"
INCLUDEPATH += "$${HOMEDIR}/.platformio/packages/framework-arduinoststm32/STM32F1/libraries/Wire"
INCLUDEPATH += "$${HOMEDIR}/.platformio/packages/toolchain-gccarmnoneeabi/arm-none-eabi/include"
INCLUDEPATH += "$${HOMEDIR}/.platformio/packages/toolchain-gccarmnoneeabi/lib/gcc/arm-none-eabi/4.8.4/include"
INCLUDEPATH += "$${HOMEDIR}/.platformio/packages/toolchain-gccarmnoneeabi/lib/gcc/arm-none-eabi/4.8.4/include-fixed"

DEFINES += "F_CPU=72000000L"
DEFINES += "GENERIC_STM32F103C"
DEFINES += "PLATFORMIO=30301"
DEFINES += "ARDUINO_GENERIC_STM32F103C"
DEFINES += "MCU_STM32F103CB"
DEFINES += "CONFIG_MAPLE_MINI_NO_DISABLE_DEBUG=1"
DEFINES += "ARDUINO=10610"
DEFINES += "BOARD_generic_stm32f103c"
DEFINES += "ERROR_LED_PORT=GPIOB"
DEFINES += "ERROR_LED_PIN=1"
DEFINES += "DEBUG_LEVEL=DEBUG_NONE"
DEFINES += "__STM32F1__"
DEFINES += "ARDUINO_ARCH_STM32F1"
DEFINES += "VECT_TAB_ADDR=134217728"

OTHER_FILES += platformio.ini

HEADERS += src/battery.h \
    lib/Adafruit-GFX-Library/Fonts/DroidSans.h \
    lib/Adafruit-GFX-Library/Fonts/DroidSans16.h \
    lib/Adafruit-GFX-Library/Fonts/DroidSans16b.h \
    lib/Adafruit-GFX-Library/Fonts/DroidSans20.h \
    lib/Adafruit-GFX-Library/Fonts/DroidSans26.h \
    lib/Adafruit-GFX-Library/Fonts/DroidSans32pt7b.h \
    lib/Adafruit-GFX-Library/Fonts/DroidSans7.h \
    lib/Adafruit-GFX-Library/Fonts/DroidSans9.h \
    lib/Adafruit-GFX-Library/Fonts/DroidSansBold32pt7b.h \
    lib/Adafruit-GFX-Library/Fonts/FreeMono12pt7b.h \
    lib/Adafruit-GFX-Library/Fonts/FreeMono18pt7b.h \
    lib/Adafruit-GFX-Library/Fonts/FreeMono24pt7b.h \
    lib/Adafruit-GFX-Library/Fonts/FreeMono9pt7b.h \
    lib/Adafruit-GFX-Library/Fonts/FreeMonoBold12pt7b.h \
    lib/Adafruit-GFX-Library/Fonts/FreeMonoBold18pt7b.h \
    lib/Adafruit-GFX-Library/Fonts/FreeMonoBold24pt7b.h \
    lib/Adafruit-GFX-Library/Fonts/FreeMonoBold9pt7b.h \
    lib/Adafruit-GFX-Library/Fonts/FreeMonoBoldOblique12pt7b.h \
    lib/Adafruit-GFX-Library/Fonts/FreeMonoBoldOblique18pt7b.h \
    lib/Adafruit-GFX-Library/Fonts/FreeMonoBoldOblique24pt7b.h \
    lib/Adafruit-GFX-Library/Fonts/FreeMonoBoldOblique9pt7b.h \
    lib/Adafruit-GFX-Library/Fonts/FreeMonoOblique12pt7b.h \
    lib/Adafruit-GFX-Library/Fonts/FreeMonoOblique18pt7b.h \
    lib/Adafruit-GFX-Library/Fonts/FreeMonoOblique24pt7b.h \
    lib/Adafruit-GFX-Library/Fonts/FreeMonoOblique9pt7b.h \
    lib/Adafruit-GFX-Library/Fonts/FreeSans12pt7b.h \
    lib/Adafruit-GFX-Library/Fonts/FreeSans18pt7b.h \
    lib/Adafruit-GFX-Library/Fonts/FreeSans24pt7b.h \
    lib/Adafruit-GFX-Library/Fonts/FreeSans9pt7b.h \
    lib/Adafruit-GFX-Library/Fonts/FreeSansBold12pt7b.h \
    lib/Adafruit-GFX-Library/Fonts/FreeSansBold18pt7b.h \
    lib/Adafruit-GFX-Library/Fonts/FreeSansBold24pt7b.h \
    lib/Adafruit-GFX-Library/Fonts/FreeSansBold9pt7b.h \
    lib/Adafruit-GFX-Library/Fonts/FreeSansBoldOblique12pt7b.h \
    lib/Adafruit-GFX-Library/Fonts/FreeSansBoldOblique18pt7b.h \
    lib/Adafruit-GFX-Library/Fonts/FreeSansBoldOblique24pt7b.h \
    lib/Adafruit-GFX-Library/Fonts/FreeSansBoldOblique9pt7b.h \
    lib/Adafruit-GFX-Library/Fonts/FreeSansOblique12pt7b.h \
    lib/Adafruit-GFX-Library/Fonts/FreeSansOblique18pt7b.h \
    lib/Adafruit-GFX-Library/Fonts/FreeSansOblique24pt7b.h \
    lib/Adafruit-GFX-Library/Fonts/FreeSansOblique9pt7b.h \
    lib/Adafruit-GFX-Library/Fonts/FreeSerif12pt7b.h \
    lib/Adafruit-GFX-Library/Fonts/FreeSerif18pt7b.h \
    lib/Adafruit-GFX-Library/Fonts/FreeSerif24pt7b.h \
    lib/Adafruit-GFX-Library/Fonts/FreeSerif9pt7b.h \
    lib/Adafruit-GFX-Library/Fonts/FreeSerifBold12pt7b.h \
    lib/Adafruit-GFX-Library/Fonts/FreeSerifBold18pt7b.h \
    lib/Adafruit-GFX-Library/Fonts/FreeSerifBold24pt7b.h \
    lib/Adafruit-GFX-Library/Fonts/FreeSerifBold9pt7b.h \
    lib/Adafruit-GFX-Library/Fonts/FreeSerifBoldItalic12pt7b.h \
    lib/Adafruit-GFX-Library/Fonts/FreeSerifBoldItalic18pt7b.h \
    lib/Adafruit-GFX-Library/Fonts/FreeSerifBoldItalic24pt7b.h \
    lib/Adafruit-GFX-Library/Fonts/FreeSerifBoldItalic9pt7b.h \
    lib/Adafruit-GFX-Library/Fonts/FreeSerifItalic12pt7b.h \
    lib/Adafruit-GFX-Library/Fonts/FreeSerifItalic18pt7b.h \
    lib/Adafruit-GFX-Library/Fonts/FreeSerifItalic24pt7b.h \
    lib/Adafruit-GFX-Library/Fonts/FreeSerifItalic9pt7b.h \
    lib/Adafruit-GFX-Library/Fonts/Orbitron.h \
    lib/Adafruit-GFX-Library/Fonts/Org_01.h \
    lib/Adafruit-GFX-Library/Fonts/Picopixel.h \
    lib/Adafruit-GFX-Library/Fonts/TomThumb.h \
    lib/Adafruit-GFX-Library/Fonts/Ubuntu-C-Big.h \
    lib/Adafruit-GFX-Library/Fonts/Ubuntu-C13.h \
    lib/Adafruit-GFX-Library/Adafruit_GFX.h \
    lib/Adafruit-GFX-Library/gfxfont.h \
    lib/Adafruit_SSD1306/Adafruit_SSD1306.h \
    lib/MenuSystem/examples/serial_nav/CustomNumericMenuItem.h \
    lib/MenuSystem/examples/serial_nav/MyRenderer.h \
    lib/MenuSystem/MenuSystem.h \
    lib/Metro/Metro.h \
    src/battery.h \
    src/components.h \
    src/definitions.h \
    src/odometer.h \
    src/temperature.h \
    src/ui.h \
    lib/STM32_Sleep/STM32_Sleep.h \
    src/config.h
HEADERS += src/ui.h
SOURCES += src/main.ino \
    lib/Adafruit-GFX-Library/Adafruit_GFX.cpp \
    lib/Adafruit_SSD1306/Adafruit_SSD1306.cpp \
    lib/MenuSystem/examples/serial_nav/CustomNumericMenuItem.cpp \
    lib/MenuSystem/examples/serial_nav/MyRenderer.cpp \
    lib/MenuSystem/MenuSystem.cpp \
    lib/Metro/Metro.cpp \
    src/battery.cpp \
    src/odometer.cpp \
    src/temperature.cpp \
    src/ui.cpp \
    src/ui_config.cpp \
    lib/Adafruit-GFX-Library/fontconvert/fontconvert.c \
    lib/Adafruit-GFX-Library/glcdfont.c \
    lib/STM32_Sleep/STM32_Sleep.cpp \
    src/config.cpp
SOURCES += src/ui.cpp
HEADERS += src/temperature.h
HEADERS += src/odometer.h
HEADERS += src/definitions.h
SOURCES += src/battery.cpp
HEADERS += src/components.h
SOURCES += src/ui_config.cpp
SOURCES += src/temperature.cpp
SOURCES += src/odometer.cpp

DISTFILES += \
    lib/Adafruit-GFX-Library/fontconvert/fontconvert_win.md \
    lib/Adafruit-GFX-Library/README.md \
    lib/Adafruit_SSD1306/README.md \
    lib/MenuSystem/examples/led_matrix/README.md \
    lib/MenuSystem/examples/led_matrix_animated/README.md \
    lib/MenuSystem/README.md \
    lib/Adafruit-GFX-Library/license.txt \
    lib/Adafruit_SSD1306/license.txt \
    lib/Adafruit_SSD1306/README.txt \
    lib/MenuSystem/keywords.txt \
    lib/Metro/about.txt \
    lib/Metro/keywords.txt \
    lib/readme.txt \
    README.txt \
    lib/Adafruit-GFX-Library/fontconvert/Makefile \
    lib/MenuSystem/examples/current_item/Makefile \
    lib/MenuSystem/examples/current_menu/Makefile \
    lib/MenuSystem/examples/lcd_nav/Makefile \
    lib/MenuSystem/examples/led_matrix/Makefile \
    lib/MenuSystem/examples/led_matrix_animated/Makefile \
    lib/MenuSystem/examples/pcd8544_nav/Makefile \
    lib/MenuSystem/examples/serial_nav/Makefile

#ifndef TEMPERATURE_H
#define TEMPERATURE_H


class Temperature
{
  public:
    Temperature(void);

    void setup(void);

    void update(void);
    float get_temperature(void); // [C]

  private:
    float temperature  = 0.0f;
};

#endif // TEMPERATURE_H

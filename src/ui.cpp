#include "components.h"

#include <TimeLib.h>
#include <Fonts/DroidSans7.h>
#include <Fonts/DroidSans9.h>
#include <Fonts/DroidSans16.h>
#include <Fonts/Ubuntu-C13.h>
#include <Fonts/Ubuntu-C-Big.h>
#include <Metro.h>



#define SPLASH_DURATION 1500

/**
 * TODO:
 *  * implement subpages using other two buttons, remember internal page for each page
 *  * smooth transitions
 *  * icons
 *  * better fonts
 */

UI::UI(void)
  : config_menu(null_renderer)
{
  current_page = previous_page = Page::SPLASH; // TODO: go straight to main page based on config
  full_refresh = true;
}

void UI::setup(void)
{
  display.begin(SSD1306_SWITCHCAPVCC, SSD1306_I2C_ADDRESS);  // initialize with the I2C addr 0x3D (for the 128x64)

  /* apply configuration */
  if (g_components->config.data.UI.dimmed_mode)
    display.dim(true);
  else
    display.dim(false); // maximum contrast

  if (g_components->config.data.UI.inverted_mode)
    display.invertDisplay(true);

  set_time_to_sleep();
  set_time_to_standby();

  display.clearDisplay();
  display.setTextColor(WHITE);

  create_config_menu();
}

void UI::test(void)
{
  // test
  display.fillScreen(1);
  display.display();
}

void UI::update(void)
{
  /* see if we can go to sleep or we should stay awake */
  power_management();

  /* process current page */
  Page page_before = current_page;

  bool should_refresh = full_refresh;
  if (should_refresh)
  {
    display.clearDisplay();
  }

  switch(current_page)
  {
    case Page::SPLASH:
      on_page_splash();
    break;
    case Page::SPEED:
      on_page_speed();
    break;
    case Page::DISTANCE:
      on_page_distance();
    break;
    case Page::ELAPSED_TIME:
      on_page_elapsed();
    break;
    case Page::CLOCK:
      on_page_clock();
    break;
    case Page::BATTERY:
      on_page_battery();
    break;
    case Page::TEMPERATURE:
      on_page_temperature();
    break;
    case Page::CONFIG:
      on_page_config();
    break;
    default:
    break;
  }

  if (should_refresh)
  {
    display.display();
  }

  if (current_page != Page::CONFIG && button_is_held(ButtonName::BOTTOM_RIGHT, 1000))
  {
    change_page(Page::CONFIG);
  }

  /* if while processing current page, the page was changed, clear memory and force full refresh next time */
  if (page_before != current_page)
  {
    full_refresh = true;
  }
}

void UI::power_management(void)
{
  /* check for activity (buttons pressed or moved) */
  bool activity_detected = g_components->odometer.moved();
  for (int i = 0; i < 4 && !activity_detected; i++)
  {
    if (button_changed((ButtonName)i)) activity_detected = true;
  }

  /* decide if go to sleep or come back from sleep */
  if (!sleeping)
  {
    if (!activity_detected)
    {
      if (sleep_timer.check())
      {
        sleeping = true;
        display.power(false);
        STM32_Sleep::stop();
        return;
      }
    }
    else
    {
      sleep_timer.reset();
      update_standby_time();
    }
  }
  else
  {
    if (activity_detected)
    {
      sleeping = false;
      display.power(true);
      return;
    }
    /* we might be here if RTC wakes us up to check for standby time */
    else
    {
      if (should_standby())
      {
        /* TODO: draw something here to indicate going into standby */
        g_components->config.save();
        STM32_Sleep::standby();
      }
      /* something else woke us up, go back to sleep */
      else
      {
        STM32_Sleep::stop();
        return;
      }
    }
  }
}


void UI::on_page_splash(void)
{
  static Metro switch_timer(SPLASH_DURATION, true);

  if (full_refresh)
  {
    display.setFont(&DroidSans9pt7b);
    display.setCursor(0, 18);
    display.print("Bicycle");
    display.setCursor(13, 40);
    display.print("Companion");
    full_refresh = false;
  }

  if (switch_timer.check()) change_page(Page::SPEED); // TODO: go to "initial page" set by user
}

void UI::on_page_speed(void)
{
  // TODO: redraw when number changes
  if (full_refresh)
  {
    float velocity = g_components->odometer.get_velocity();
    int decimal = (int)velocity;
    float fraction = (velocity - decimal);

    char buffer[3];

    display.setFont(&Ubuntu_C36pt7b);
    display.setCursor(32,61);
    sprintf(buffer, "%.2i", decimal);
    display.print(buffer);
    display.setFont(&Ubuntu_C13pt7b);
    display.setCursor(display.getCursorX() + 5,61);
    sprintf(buffer, "%.1i ", (int)(fraction * 10));
    display.print(buffer);

    size_t bar_width = 6;
    size_t bar_y_offset = 10;
    float p = g_components->odometer.get_velocity_percent();
    size_t bar_height = display.height() - bar_y_offset;
    size_t bar_percent_height = (size_t)(bar_height * p);

    display.drawRect(display.width() - bar_width, bar_y_offset, bar_width, bar_height, 1);
    display.fillRect(display.width() - bar_width, (bar_height - bar_percent_height) + bar_y_offset, bar_width, bar_percent_height, 1);

    // major tick marks
    size_t tick_y = bar_y_offset;
    display.drawLine(display.width() - bar_width - 2, tick_y, display.width() - bar_width, tick_y, 1);
    tick_y = (size_t)((display.height() - bar_y_offset) * 0.25) + bar_y_offset;
    display.drawLine(display.width() - bar_width - 2, tick_y, display.width() - bar_width, tick_y, 1);
    tick_y = (size_t)((display.height() - bar_y_offset) * 0.50) + bar_y_offset;
    display.drawLine(display.width() - bar_width - 2, tick_y, display.width() - bar_width, tick_y, 1);
    tick_y = (size_t)((display.height() - bar_y_offset) * 0.75) + bar_y_offset;
    display.drawLine(display.width() - bar_width - 2, tick_y, display.width() - bar_width, tick_y, 1);
    tick_y = display.height() - 1;
    display.drawLine(display.width() - bar_width - 2, tick_y, display.width() - bar_width, tick_y, 1);

    // TODO: draw speed-bar at the bottom or right, use some tick marks maybe

    full_refresh = false;
  }
  else if (g_components->odometer.was_updated())
  {
    full_refresh = true;
  }

  if (button_was_pressed(ButtonName::TOP_RIGHT)) change_page(Page::DISTANCE);
}

void UI::on_page_distance(void)
{
  if (full_refresh)
  {
    /* round to two decimals */
    float distance = g_components->odometer.get_distance();
    distance = roundf(distance * 100.0f) / 100.0f;

    int decimal = (int)distance;
    int fraction = (int)((distance - decimal) * 100);

#if 0
    Serial3.print("d: "); Serial3.print(distance);
    Serial3.printf(" %i . %i\n", decimal, fraction);
#endif

    display.setFont(&Ubuntu_C36pt7b);

    char buffer[6];

    if (decimal < 1000)
    {
      display.setCursor(13,61);
      sprintf(buffer,"%3i", decimal);
      display.print(buffer);
      display.setFont(&DroidSans9pt7b);
      display.setCursor(display.getCursorX(),61);
      sprintf(buffer, "%.2i ", fraction);
      display.print(buffer);
    }
    else if (decimal < 10000)
    {
      display.setCursor(7,61);
      sprintf(buffer, "%4i", decimal);
      display.print(buffer);
    }
    else {
      display.setCursor(0,61);
      sprintf(buffer, "%4i", 9999); // TODO: use smaller font and display bigger numbers
      display.print(buffer);
    }

    full_refresh = false;
  }
  else if (g_components->odometer.was_updated())
  {
    full_refresh = true;
  }

  if (button_was_pressed(ButtonName::TOP_LEFT)) change_page(Page::SPEED);
  else if (button_was_pressed(ButtonName::TOP_RIGHT)) change_page(Page::ELAPSED_TIME);
}

void UI::on_page_elapsed(void)
{
  if (full_refresh)
  {
    display.setFont(&DroidSans16pt7b);
    display.setCursor(5,30);
    display.print("elapsed");

    full_refresh = false;
  }

  if (button_was_pressed(ButtonName::TOP_LEFT)) change_page(Page::DISTANCE);
  else if (button_was_pressed(ButtonName::TOP_RIGHT)) change_page(Page::CLOCK);
}

void UI::on_page_clock(void)
{
  static Metro refresh_timer(500, true);

  if (full_refresh)
  {
    TimeElements t;
    breakTime(g_components->rtc.getTime(), t);

    char buffer[16];

    display.setFont(&DroidSans16pt7b);
    display.setCursor(5, 30);
    sprintf(buffer, "%.2i:%.2i:%.2i\n", t.Hour, t.Minute, t.Second);
    display.print(buffer);
    display.setFont(&DroidSans7pt7b);
    display.setCursor(15, 55);
    sprintf(buffer, " %.2i . %s . %.2i", t.Day, monthShortStr(t.Month), (1970 + t.Year) % 100);
    display.print(buffer);

    full_refresh = false;
  }

  if (button_was_pressed(ButtonName::TOP_LEFT)) change_page(Page::ELAPSED_TIME);
  else if (button_was_pressed(ButtonName::TOP_RIGHT)) change_page(Page::BATTERY);

  // update clock display at 2Hz (avoid skipping seconds at 1Hz)
  if (refresh_timer.check())
  {
    full_refresh = true;
  }
}

void UI::on_page_battery(void)
{
  static Metro refresh_timer(200, true);

  if (full_refresh)
  {
    char buffer[5];
    display.setFont(&DroidSans16pt7b);
    display.setCursor(35, 30);
    sprintf(buffer, "%.2i %%", (int)g_components->battery.get_charge());
    display.print(buffer);
    display.setFont(&DroidSans9pt7b);
    display.setCursor(45, 55);
    display.print(g_components->battery.get_voltage());
    display.print(" v");

    full_refresh = false;
  }

  if (button_was_pressed(ButtonName::TOP_LEFT)) change_page(Page::CLOCK);
  else if (button_was_pressed(ButtonName::TOP_RIGHT)) change_page(Page::TEMPERATURE);

  if (refresh_timer.check())
  {
    full_refresh = true;
  }
}

void UI::on_page_temperature(void)
{
  if (full_refresh)
  {
    display.setFont(&DroidSans16pt7b);
    display.setCursor(20,40);
    display.print(g_components->temperature.get_temperature(), 1);
    display.print(" C");

    full_refresh = false;
  }

  if (button_was_pressed(ButtonName::TOP_LEFT)) change_page(Page::BATTERY);
}

void UI::change_page(UI::Page new_page)
{
#if 0
  Serial.printf("page: %i -> %i\n", current_page, new_page);
#endif
  previous_page = current_page;
  current_page = new_page;
}

bool UI::button_was_pressed(ButtonName button_name)
{
  return g_components->buttons[(int)button_name].wasPressed();
}

bool UI::button_changed(ButtonName button_name)
{

  return (g_components->buttons[(int)button_name].wasPressed() ||
          g_components->buttons[(int)button_name].wasReleased());
}

bool UI::button_is_held(ButtonName button_name, int ms)
{
  return g_components->buttons[(int)button_name].pressedFor(ms);
}

bool UI::should_standby(void)
{
  return  (standby_time != 0 && g_components->rtc.getTime() >= standby_time);
}

Adafruit_SSD1306& UI::get_display()
{
  return display;
}

void UI::update_standby_time(void)
{
  if (g_components->config.data.UI.time_to_standby == 0) standby_time = 0;
  else
  {
    standby_time = g_components->rtc.getTime() + g_components->config.data.UI.time_to_standby * 60;
    rtc_set_alarm(standby_time);
  }
}

void UI::set_time_to_sleep(void)
{
  sleep_timer.interval(g_components->config.data.UI.time_to_sleep * 1000);
}

void UI::set_time_to_standby(void)
{
  update_standby_time();
}



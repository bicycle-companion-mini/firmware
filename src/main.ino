/********************************************************************************************
 * Include files
 ********************************************************************************************/

#include "components.h"

/**
 * TODO (*considerar adaptacion a STM32F103):
 * * Config en EEPROM
 * * Sensor temperatura (DS18B20)
 * * Alimentacion por bateria (*)
 * * Sleep (*) -> sleep uC while keeping screen on displaying time (maybe also dim) and wake only to update once a minute.
 *    maybe this can be extended to all screens and wake only when underlying data changes.
 *    Depending on screen different sleep is possible: speed/distance allow SLEEP state (if timer is used to count pulses),
 *    wakes every second are only needed. For longer timeout STOP can be used (optionally tuning screen before that)
 *    Remember to map some button to WKUP pin so that awaking from STANDBY is possible
 */

/********************************************************************************************
 * Defines
 ********************************************************************************************/

#define ENABLE_SERIAL 1

/********************************************************************************************
 * Components
 ********************************************************************************************/
Components components;
Components* g_components = &components;

/********************************************************************************************
 * Static definitions
 ********************************************************************************************/

static void noop_isr(void) { }

/********************************************************************************************
 * Setup
 ********************************************************************************************/

void setup()
{  
  STM32_Sleep::init();

  /* init led */
  pinMode(LED_PIN, OUTPUT);
  digitalWrite(LED_PIN, HIGH);

#if ENABLE_SERIAL
  /* setup serial */
  Serial.begin(115200);
#endif

  /* setup other components */
  components.config.setup();
  components.odometer.setup();
  components.ui.setup();

  /* attach interrupts to buttons so that we awake from sleep with them */
  attachInterrupt(BUTTON_BL, awake_button_isr, &components.buttons[(int)ButtonName::BOTTOM_LEFT], RISING);
  attachInterrupt(BUTTON_BR, awake_button_isr, &components.buttons[(int)ButtonName::BOTTOM_RIGHT], RISING);
  attachInterrupt(BUTTON_TL, awake_button_isr, &components.buttons[(int)ButtonName::TOP_LEFT], RISING);
  attachInterrupt(BUTTON_TR, awake_button_isr, &components.buttons[(int)ButtonName::TOP_RIGHT], RISING);

  /* this leaves the RTC alarm configure so that only the alarm time needs to be reset */
  components.rtc.createAlarm(&noop_isr, g_components->rtc.getTime());
  //components.rtc.attachSecondsInterrupt(&seconds_isr);
}

/********************************************************************************************
 * Loop
 ********************************************************************************************/

void loop(void)
{
  for (int i = 0; i < (int)ButtonName::COUNT; i++)
    components.buttons[i].read();

  components.ui.update();
  components.battery.update();
  components.odometer.update();
}

void awake_button_isr(void* arg)
{
  // TODO: this should update state of Button, to avoid polling
}

#include <Arduino.h>
#include <Metro.h>
#include "components.h"

#define HALL_SENSOR_PIN PA15
#define MAXIMUM_VELOCITY 45 // TODO: user defined via config

volatile static int32_t tick_count = 0;
void hall_sensor_isr(void);

Odometer::Odometer(void)
{
  last_time = 0;
  distance = total_distance = 0.0f; // TODO: get from EEPROM
  velocity = 0.0f;
}

void Odometer::setup(void)
{
  pinMode(HALL_SENSOR_PIN, INPUT_PULLUP);
  attachInterrupt(HALL_SENSOR_PIN, &hall_sensor_isr, CHANGE);
}

void Odometer::update(void)
{
  static Metro update_timer(1000, true);

  if (update_timer.check())
  {
    int32_t now = millis();

    if (last_time != 0)
    {
      // get ticks and quickly reset
      noInterrupts();
      int32_t tick_count_copy = tick_count;
      tick_count = 0;
      interrupts();

      updated = true;

      if (tick_count_copy == 0) moved_flag = false;
      else moved_flag = true;

      // each tick is half revolution, compute traveled distance in km
      float delta_distance = tick_count_copy * (g_components->config.data.Odometer.wheel_perimeter / 1e6f) * 0.5f;
      distance += delta_distance;

      // velocity as traveled distance in last period over time difference (ms -> hr)
      velocity = (delta_distance / ((now - last_time) * 1e-3f)) * 3600.0f;
      //velocity = (delta_distance * 1000.0f) / ((now - last_time) * 1e-3f); // m/s
    }

    last_time = now;
  }
  else
    updated = false;
}

float Odometer::get_distance(void)
{
  return distance;
}

float Odometer::get_velocity()
{
  return velocity;
}

float Odometer::get_velocity_percent()
{
  return velocity / MAXIMUM_VELOCITY;
}

float Odometer::get_total_distance()
{
  return total_distance;
}

bool Odometer::was_updated(void)
{
  return updated;
}

bool Odometer::moved(void)
{
  return moved_flag;
}

void hall_sensor_isr(void)
{
  tick_count++;
}

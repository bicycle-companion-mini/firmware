#ifndef UI_H
#define UI_H

#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#include <MenuSystem.h>
#include <Metro.h>
#include "definitions.h"

class UI
{
  public:
    UI(void);

    void setup(void);
    void update(void);  /* refreshes the display, processes user input */
    void test(void);

    enum class Page {
      SPLASH,
      SPEED, DISTANCE, ELAPSED_TIME,
      CLOCK, BATTERY, TEMPERATURE,
      CONFIG
    };

    void exit_config(void);

    Adafruit_SSD1306& get_display(void);

    /* some configs */
    void set_time_to_sleep(void);
    void set_time_to_standby(void);

  private:
    Adafruit_SSD1306 display;
    Page current_page, previous_page;
    bool full_refresh;

    void refresh(void);
    void power_management(void);

    void on_page_splash(void);
    void on_page_speed(void);
    void on_page_distance(void);
    void on_page_elapsed(void);
    void on_page_clock(void);
    void on_page_battery(void);
    void on_page_temperature(void);
    void on_page_config(void);

    void change_page(Page new_page);
    bool button_was_pressed(ButtonName button_name);
    bool button_changed(ButtonName button_name);
    bool button_is_held(ButtonName button_name, int ms);

    Metro sleep_timer = Metro(0, true);
    time_t standby_time = 0;

    bool should_standby(void);
    void update_standby_time(void);

    bool sleeping = false;

    class NullRenderer : public MenuComponentRenderer
    {
      public:
        void render(Menu const& menu) const { };

        void render_menu_item(MenuItem const& menu_item) const { };
        void render_back_menu_item(BackMenuItem const& menu_item) const { };
        void render_numeric_menu_item(NumericMenuItem const& menu_item) const { };
        void render_menu(Menu const& menu) const { };
    };

    NullRenderer null_renderer;
    MenuSystem config_menu;
    void create_config_menu(void);
};

#endif // UI_H

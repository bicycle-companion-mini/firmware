#include <Metro.h>
#include "components.h"


class BooleanMenuItem : public MenuItem
{
  public:
    BooleanMenuItem(const char* name, SelectFnPtr select_fn, bool default_value) :
      MenuItem(name, select_fn), value(default_value)
    { }

    bool get_value(void) const { return value; }

    virtual void render(MenuComponentRenderer const& renderer) const
    {
      /* render checkbox */
      Adafruit_SSD1306& display = g_components->ui.get_display();

      size_t box_size = 7;
      size_t y_offset = 1;

      display.drawRect(display.width() - box_size, display.getCursorY() + y_offset, box_size, box_size, 1);
      if (value)
        display.fillRect(display.width() - box_size + 2, display.getCursorY() + 2 + y_offset, box_size - 4, box_size - 4, 1);

      /*size_t x = display.getCursorX();
      size_t y = display.getCursorY();
      display.setCursor(display.width() - 20, y);
      display.print(value ? "[x]" : "[ ]");
      display.setCursor(x, y);*/
    }

  protected:
    virtual Menu* select()
    {
      value = !value;

      if (_select_fn != nullptr)
        _select_fn(this);

      return nullptr;
    }

  private:
    bool value;
};


static void select_exit_config(MenuItem* item)
{
  g_components->ui.exit_config();
}

static void select_exit_if_moving(MenuItem* item)
{
  g_components->config.data.UI.exit_config_if_moving = ((BooleanMenuItem*)item)->get_value();
}

static void select_wheel_perimeter(MenuItem* item)
{
  g_components->config.data.Odometer.wheel_perimeter = (int)((NumericMenuItem*)item)->get_value();
}

static void select_sleep_time(MenuItem* item)
{
  g_components->config.data.UI.time_to_sleep = ((NumericMenuItem*)item)->get_value();
  g_components->ui.set_time_to_sleep();
}

static void select_standby_time(MenuItem* item)
{
  g_components->config.data.UI.time_to_standby = ((NumericMenuItem*)item)->get_value();
  g_components->ui.set_time_to_standby();
}

static void select_dimmed_mode(MenuItem* item)
{
  g_components->config.data.UI.dimmed_mode = ((BooleanMenuItem*)item)->get_value();
  g_components->ui.get_display().dim(g_components->config.data.UI.dimmed_mode);
}

static void select_inverted_mode(MenuItem* item)
{
  g_components->config.data.UI.inverted_mode = ((BooleanMenuItem*)item)->get_value();
  g_components->ui.get_display().invertDisplay(g_components->config.data.UI.inverted_mode);
}

void UI::create_config_menu(void)
{
  Menu& main_menu = config_menu.get_root_menu();

  Menu* odometer_menu = new Menu("Odometer");
  odometer_menu->add_item(new NumericMenuItem("Wheel perimeter", &::select_wheel_perimeter, g_components->config.data.Odometer.wheel_perimeter, CONFIG_WHEEL_PERIMETER_MIN, CONFIG_WHEEL_PERIMETER_MAX));
  odometer_menu->add_item(new BooleanMenuItem("Exit cfg if moving", &::select_exit_if_moving, g_components->config.data.UI.exit_config_if_moving));
  main_menu.add_menu(odometer_menu);

  Menu* time_date_menu = new Menu("Time & date");
  time_date_menu->add_item(new BackMenuItem("... back", nullptr, &config_menu));
  main_menu.add_menu(time_date_menu);

  Menu* powersave_menu = new Menu("Powersaving");
  powersave_menu->add_item(new NumericMenuItem("Sleep time", &::select_sleep_time,  g_components->config.data.UI.time_to_sleep, CONFIG_TIME_TO_SLEEP_MIN, CONFIG_TIME_TO_SLEEP_MAX));
  powersave_menu->add_item(new NumericMenuItem("Standby time", &::select_standby_time,  g_components->config.data.UI.time_to_standby, CONFIG_TIME_TO_STANDBY_MIN, CONFIG_TIME_TO_STANDBY_MAX));
  powersave_menu->add_item(new BooleanMenuItem("Dim at night", nullptr, false));
  main_menu.add_menu(powersave_menu);

  Menu* display_menu = new Menu("Display");
  display_menu->add_item(new BooleanMenuItem("Inverted Mode", &::select_inverted_mode, g_components->config.data.UI.inverted_mode));
  display_menu->add_item(new BooleanMenuItem("Dimmed Mode", &::select_dimmed_mode, g_components->config.data.UI.dimmed_mode));
  //display_menu->add_item(new BooleanMenuItem("Invert when bright", nullptr, false));
  main_menu.add_menu(display_menu);

  MenuItem* back = new BackMenuItem("... back", &::select_exit_config, &config_menu);
  main_menu.add_item(back);
}

void UI::on_page_config(void)
{
  int separation = 2;

  if (full_refresh)
  {
    full_refresh = false;

    display.setFont(NULL);
    display.setCursor(25,0);

    if (config_menu.get_current_menu() == &config_menu.get_root_menu())
      display.println("CONFIGURATION");
    else
    {
      const char* name = (config_menu.get_current_menu()->get_current_component()->has_focus() ?
                            config_menu.get_current_menu()->get_current_component()->get_name() :
                            config_menu.get_current_menu()->get_name());

      String name_str(name);
      name_str.toUpperCase();
      display.println(name_str.c_str());
    }

    display.drawLine(0, display.getCursorY(), display.width(), display.getCursorY(), 1);
    display.setCursor(0, display.getCursorY() + 2 + separation);

    /* draw focused item alone */
    if (config_menu.get_current_menu()->get_current_component()->has_focus())
    {
      // for now only menu item type which is focuseable is numeric, in general we should call render() so that
      // inheritance takes care of deciding how to render
      const NumericMenuItem* numeric = static_cast<const NumericMenuItem*>(config_menu.get_current_menu()->get_current_component());

      display.print(numeric->get_value_string().c_str());
    }
    /* draw menu */
    else
    {
      int current_item = config_menu.get_current_menu()->get_current_component_num();
      int child_items = config_menu.get_current_menu()->get_num_components();

      int items_per_page = 5;


      int first_i = max(0, current_item - (items_per_page - 1));
      int max_items = min(first_i + items_per_page, child_items);

      for (int i = first_i; i < max_items; i++)
      {
        const MenuComponent* child = config_menu.get_current_menu()->get_menu_component(i);

        if (child->is_current())
          display.print("> ");
        else
          display.print("  ");

        display.print(child->get_name());
        child->render(null_renderer); /* let item draw any extra stuff */
        display.println("");
        display.setCursor(display.getCursorX(), display.getCursorY() + separation);
      }
    }
  }

  if (g_components->config.data.UI.exit_config_if_moving && g_components->odometer.moved())
  {
    g_components->ui.exit_config();
  }
  else if (button_is_held(ButtonName::TOP_LEFT, 1000))
  {
    static Metro repeat_timer(10, true);
    if (repeat_timer.check())
    {
      config_menu.prev();
      full_refresh = true;
    }
  }
  else if (button_is_held(ButtonName::BOTTOM_LEFT, 1000))
  {
    static Metro repeat_timer(10, true);
    if (repeat_timer.check())
    {
      config_menu.next();
      full_refresh = true;
    }
  }
  else if (button_was_pressed(ButtonName::TOP_LEFT))
  {
    config_menu.prev();
    full_refresh = true;
  }
  else if (button_was_pressed(ButtonName::BOTTOM_LEFT))
  {
    config_menu.next();
    full_refresh = true;
  }
  else if (button_was_pressed(ButtonName::TOP_RIGHT))
  {
    config_menu.select();
    full_refresh = true;
  }
  else if (button_was_pressed(ButtonName::BOTTOM_RIGHT))
  {
    /* focus-eable items are deselected using select(), not back() */
    if (config_menu.get_current_menu()->get_current_component()->has_focus())
      config_menu.select();
    else
      config_menu.back();

    full_refresh = true;
  }
}

void UI::exit_config(void)
{
  g_components->config.save();
  while (config_menu.back()) { }; /* hack to get to main config menu */
  change_page(previous_page);
}

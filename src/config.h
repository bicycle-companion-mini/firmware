#ifndef CONFIG_H
#define CONFIG_H

#define CONFIG_WHEEL_PERIMETER_MAX 3000
#define CONFIG_WHEEL_PERIMETER_MIN 0

#define CONFIG_TIME_TO_SLEEP_MIN 0
#define CONFIG_TIME_TO_SLEEP_MAX 3600

#define CONFIG_TIME_TO_STANDBY_MIN 0
#define CONFIG_TIME_TO_STANDBY_MAX 1440 // 24hs

class Config
{
  public:
    Config(void);

    void setup(void);
    void save(void);

    struct {
      struct
      {
        bool exit_config_if_moving = true;
        int time_to_sleep = 60;  // [s]
        int time_to_standby = 2; // [m]
        bool inverted_mode = false;
        bool dimmed_mode = false;
      } UI;

      struct
      {
        int wheel_perimeter = 1200;   // [mm]
      } Odometer;
    } data;
};

#endif // CONFIG_H

#ifndef ODOMETER_H
#define ODOMETER_H

class Odometer
{
  public:
    Odometer(void);

    void setup(void);
    void update(void); /* should be called periodically, maybe around each second */

    float get_distance(void);
    float get_velocity(void);
    float get_velocity_percent(void); // [0,1] relative to user defined max speed
    float get_total_distance(void);

    bool was_updated(void);
    bool moved(void);  /* if any pulse was detected during last update */

  private:
    int32_t last_time;
    float distance;           // [km]
    float velocity;           // [km/h]
    // other: traveled time, moving time

    /* long-term statistics */
    float total_distance;     // [km]
    float avg_velocity;
    float max_velocity, min_velocity;

    bool updated = false;
    bool moved_flag = false;
};

#endif // ODOMETER_H

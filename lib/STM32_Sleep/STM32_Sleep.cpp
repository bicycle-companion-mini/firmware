#include <libmaple/scb.h>
#include <libmaple/pwr.h>
#include <libmaple/systick.h>
#include <libmaple/gpio.h>
#include <libmaple/libmaple.h>
#include <RTClock.h>
#include "STM32_Sleep.h"
#include <Arduino.h>

/* TODO:
 * actually entering SLEEP requires no interrupts except external interrupts, i'm still getting instantly awaken. USB?
 *  reduce clock, disable peripherals, set pins to ADC, wake from RTC
*/

void STM32_Sleep::stop(void)
{
  set_mode(Mode::STOP);
}

void STM32_Sleep::init(void)
{
  /* allow use of PA0 as GPIO until standby is requested */
  PWR_BASE->CSR &= ~PWR_CSR_EWUP;
}

void STM32_Sleep::sleep(void)
{
  set_mode(Mode::SLEEP);
}

void STM32_Sleep::standby(void)
{
  set_mode(Mode::STANDBY);
}

void STM32_Sleep::set_mode(STM32_Sleep::Mode mode)
{
  /* SLEEP mode */
  if (mode == SLEEP)
  {
    /* not a deep sleep */
    SCB_BASE->SCR &= ~SCB_SCR_SLEEPDEEP;

    /* disable systick, otherwise an interrupt will wake us in the next ms */
    systick_disable();
  }
  else
  {
    /* STOP and STANDBY need the DEEPSLEEP bit */
    SCB_BASE->SCR |= SCB_SCR_SLEEPDEEP;

    /* STOP mode */
    if (mode == STOP || mode == STOP_FAST_WAKEUP)
    {
      /* low power deep-sleep off (standby) */
      PWR_BASE->CR &= ~PWR_CR_PDDS;

      if (mode == STOP_FAST_WAKEUP)
      {
        /* NO low power 1.8v regulator mode (wakes up faster) */
        PWR_BASE->CR &= ~PWR_CR_LPDS;
      }
      else
      {
        /* select low power 1.8v regulator mode */
        PWR_BASE->CR |= PWR_CR_LPDS;
      }

      /* TODO: clear all EXTI pending and RTC flags. should I disabled interrupts? */
    }
    /* STANDBY mode */
    else
    {
      /* low power deep-sleep on (standby) */
      PWR_BASE->CR |= PWR_CR_PDDS;

      /* set WKUP pin to input pull-down and wake up from rising edge on this pin */
      PWR_BASE->CSR |= PWR_CSR_EWUP;
    }
  }

  /* clear previous wakeup and standby register */
  PWR_BASE->CR |= PWR_CR_CWUF;
  PWR_BASE->CR |= PWR_CR_CSBF;

  /* clear interrupt flags which may have been set during this function call */
  *bb_perip(&EXTI_BASE->PR, EXTI_RTC_ALARM_BIT) = 1;

  /* go to sleep, wake up on interrupt */
  asm("    wfi");

  /* restore status */
  if (mode == SLEEP)
  {
    systick_enable();
  }
  else if (mode == STOP || mode == STOP_FAST_WAKEUP)
  {
    /* go back no normal clock speed */
    reset_clock();
    //reenumerate_usb(); // does not seem necessary, USB functions normally when going back. maybe if I disable usb this is needed
  }
}

void STM32_Sleep::reset_clock(void)
{
  rcc_turn_on_clk(RCC_CLK_HSI);
  while (!rcc_is_clk_ready(RCC_CLK_HSI))
      ;
  rcc_switch_sysclk(RCC_CLKSRC_HSI);
  rcc_turn_off_clk(RCC_CLK_HSE);
  rcc_turn_off_clk(RCC_CLK_PLL);
  rcc_clk_init(RCC_CLKSRC_PLL, RCC_PLLSRC_HSE, RCC_PLLMUL_9); /* careful, this multiplier may not work in all boards */
}

void STM32_Sleep::reenumerate_usb(void)
{
#ifdef SERIAL_USB

#ifdef GENERIC_BOOTLOADER
  delay(1000);
  pinMode(PA12, OUTPUT);
  digitalWrite(PA12, LOW);

  delay(1000);

  pinMode(PA12, INPUT);
#endif
  //Serial.begin();

#endif
}
